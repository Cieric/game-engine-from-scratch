#include "GameEngine/Utils/GEMalloc.h"
#include "GameEngine.h"
#include <iostream>
#include "GameEngine/Renderer/OpenGL/Renderer.h"
#include "GameEngine/Renderer/OpenGL/Shader.h"

#include "GameEngine/Objects/Components/ComponentCamera.h"
#include "GameEngine/Objects/Components/ComponentScript.h"
#include "GameEngine/Objects/Components/ComponentGraphics.h"

#include "GameEngine/Scene.h"

#include <filesystem>

#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "GameEngine/Loading/tiny_gltf.h"

#include <random>

//#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifdef GetObject
#undef GetObject
#endif

#ifdef _WIN32
#define M_PI 3.141592653589
#endif

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

static void resize_callback(GLFWwindow* window, int width, int height)
{
	Engine& engine = *(Engine*)glfwGetWindowUserPointer(window);
	engine.resized = true;
}

void showFPS(GLFWwindow *pWindow)
{
	static double lastTime = glfwGetTime();
	static int nbFrames = 0;
	
	double currentTime = glfwGetTime();
	double delta = currentTime - lastTime;
	
	nbFrames++;
	if ( delta >= 1.0 ){
		double fps = double(nbFrames) / delta;

		std::stringstream ss;
		ss << fps << " FPS";

		glfwSetWindowTitle(pWindow, ss.str().c_str());

		nbFrames = 0;
		lastTime = currentTime;
	}
}

float get_random()
{
    static std::default_random_engine e;
    static std::uniform_real_distribution<> dis(0, 1); // range 0 - 1
    return (float)dis(e);
}

bool doubleBuffered = false;

GLFWwindow *mWindow;

int main(int argc, char* argv[])
{
	if(argc > 1) {
		std::filesystem::current_path(argv[1]);
	}
	MemoryUsageInitialize();

	int glfwErr = glfwInit();
	if (glfwErr != GLFW_TRUE){
		const char* desc = nullptr;
		glfwGetError(&desc);
		Utils::PrintError("Failed to Init GLFW. {}", std::string(desc));
		return EXIT_FAILURE;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	glfwWindowHint(GLFW_DOUBLEBUFFER, doubleBuffered);

	mWindow = glfwCreateWindow(1280, 920, "OpenGL", nullptr, nullptr);

	if (mWindow == nullptr) {
		const char* desc = nullptr;
		glfwGetError(&desc);
		Utils::PrintError("Failed to Create OpenGL Context {}\n", std::string(desc));
		return EXIT_FAILURE;
	}

	glfwSetWindowSizeCallback(mWindow, resize_callback);

	Engine engine;
	glfwSetWindowUserPointer(mWindow, &engine);

	glfwMakeContextCurrent(mWindow);
	glfwSwapInterval(0);

	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::string errStr = (char*)glewGetErrorString(err);
		Utils::PrintError("Failed to init glew ({})\n", errStr);
		return EXIT_FAILURE;
	}
	
	printf("OpenGL %s\n", glGetString(GL_VERSION));
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_PROGRAM_POINT_SIZE);

	Shader shader;
	shader.LoadVertexShader("Assets/Shaders/view_phong_vert.glsl");
	shader.LoadFragmentShader("Assets/Shaders/view_phong_frag.glsl");
	shader.Build();

	shader.Bind(shader.FindBindPoint("lightPos"), Vec3f{0.f, 2.f, 0.f});
	shader.Bind(shader.FindBindPoint("lightColor"), Vec3f{1.0f, 0.77f, 0.56f});
	shader.Bind(shader.FindBindPoint("viewPos"), Vec3f{ 0.f, 0.f, 0.f });


	shader.Bind(shader.FindBindPoint("lightColor"), Vec3f{ 1.f, 1.f, 1.f });
	shader.Bind(shader.FindBindPoint("lightPos"), Vec3f{ 0.f, 0.f, 0.f });

	Scene mainScene;
	mainScene.LoadScene("main.Scene.json");

	Id_T locModel = shader.FindBindPoint("model");
	Id_T locView = shader.FindBindPoint("view");
	Id_T locProj = shader.FindBindPoint("proj");

	//Id_T locViewPos = shader.FindBindPoint("viewPos");
	
	auto MainCamera = mainScene.GetComponentHandler<ComponentCamera>().getItems()[0];
	{
		int width, height;
		glfwGetWindowSize(mWindow, &width, &height);
		shader.Bind(locProj, MainCamera.GetProjection(width, height));
		glViewport(0, 0, width, height);
	}
	double _globalTime = 0.0;
	double lastTime = glfwGetTime();

	ResetMemoryUsageStats();

	while (glfwWindowShouldClose(mWindow) == false) {

		PrintMemoryUsage();

		if (glfwGetKey(mWindow, GLFW_KEY_F10) == GLFW_PRESS)
			SetBreakpointOnLeaks();
		
		ResetMemoryUsageStats();

		_globalTime = glfwGetTime();
		float deltaTime = (float)(_globalTime - lastTime);
		lastTime = _globalTime;

		showFPS(mWindow);

		glfwPollEvents();
		
		if(engine.resized)
		{
			int width, height;
			glfwGetWindowSize(mWindow, &width, &height);
			shader.Bind(locProj, MainCamera.GetProjection(width, height));
			glViewport(0, 0, width, height);
		}

		if (glfwGetKey(mWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(mWindow, true);

		glClearColor(0.3f, 0.5f, 0.8f, 0.5f);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader.Use();

		//if (glfwGetMouseButton(mWindow, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE)
		//{
		//	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		//	//glfwGetCursorPos(mWindow, &mouse_start_x, &mouse_start_y);
		//}
		//else
		//{
		//	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		//}

		shader.Bind(locView, MainCamera.parent->getTransform());

		for (auto& compIdx : mainScene.GetComponentHandler<ComponentScript>())
		{
			compIdx.Update(deltaTime);
		}

		for (const auto& compIdx : mainScene.GetComponentHandler<ComponentGraphics>())
		{
			shader.Bind(locModel, compIdx.parent->getTransform());
			compIdx.model->Draw();
		}

		if(doubleBuffered)
			glfwSwapBuffers(mWindow);
		else
			glFinish();
	}
	
	glfwTerminate();

	return EXIT_SUCCESS;
}