#pragma once
#include <vector>
#include "Token.h"
#include "Expr.h"
#include "Stmt.h"

class Parser
{
public:
	struct Error {
		bool set;
		int pos = 0;
		int line = 0;
		std::string message;
	};

private:
	Error error = {false, 0, 0, ""};
	int current;
	std::vector<Token> tokens;

	bool hadError = false;
public:

	Parser(std::vector<Token> tokens) : current(0), tokens(tokens) {};

	//Parsing functions
	//Expr* parse();
	std::vector<Stmt*> parse();

	Error getError();

private:
	Stmt* statement();
	
	bool checkDeclType();
	Stmt* declaration();
	Stmt* varDeclaration();

	Stmt* forStatement();
	Stmt* ifStatement();
	Stmt* whileStatement();
	Stmt* printStatement();
	Stmt* returnStatement();

	std::vector<Stmt*> block();
	Stmt* expressionStatement();
	
	Expr* expression();
	Expr* assignment();
	Expr* equality();
	Expr* comparison();
	Expr* term();
	Expr* factor();
	Expr* unary();
	Expr* call();
	Expr* finishCall(Expr* callee);
	Expr* swizzle();
	Expr* primary();

	//Token Handling
	template<typename First, typename ... Args>
	bool match(First type, Args ... args);
	
	template<typename First>
	bool match(First type);

	bool check(TokenTypes type);
	Token advance();
	bool isAtEnd();
	Token peek();
	Token previous();
	Token consume(TokenTypes type, std::string message);

	//Error Handling
	void setError(int pos, std::string message);
	void synchronize();
};

template<typename First, typename ...Args>
inline bool Parser::match(First type, Args ...args)
{
	return match(type) || match(args...);
}

template<typename First>
inline bool Parser::match(First type)
{
	if (check(type)) {
		advance();
		return true;
	}
	return false;
}
