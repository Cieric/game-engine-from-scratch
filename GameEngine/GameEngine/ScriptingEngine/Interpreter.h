#pragma once
#include <fmt/printf.h>
#include <fmt/color.h>
#include <variant>
#include "Stmt.h"
#include <fmt/printf.h>
#include <fmt/color.h>

#include "Environment.h"
#include "Value.h"

class Interpreter
{
	Environment* env = new Environment();
	void* externalData = nullptr;
	bool returnFromFunc = false;
	Value returnValue;

public:
	Interpreter();

	void interpret(std::vector<Stmt*> statements);

	void pushEnv();
	void popEnv();
	Environment* getEnv();

	void execute(Stmt* stmt);
	void visitExpressionStmt(StmtExpression* stmt);
	void visitVariableStmt(StmtVar* stmt);
	Value visitAssignExpr(ExprAssign* expr);
	void visitBlockStmt(StmtBlock* stmt);
	void visitIfStmt(StmtIf* stmt);
	void visitWhileStmt(StmtWhile* stmt);
	void visitFunctionStmt(StmtFunction* stmt);

	void visitReturnStmt(StmtReturn* stmt);

	void executeBlock(std::vector<Stmt*> statements);
	static Value call(Interpreter* interpreter, Callable* func, std::vector<Value> arguments);

	Value evaluate(Expr* expr);
	Value visitLiteralExpr(ExprLiteral* expr);
	Value visitGroupingExpr(ExprGrouping* expr);
	Value visitUnaryExpr(ExprUnary* expr);
	Value visitVariableExpr(ExprVariable* expr);
	Value visitCallExpr(ExprCall* expr);
	Value visitBinaryExpr(ExprBinary* expr);
	Value visitSwizzleExpr(ExprSwizzle* expr);

	bool isTruthy(Value value);

	Value CallFunction(std::string functionName, std::vector<Value> args);
	void setData(void* data);
	void* getData();
};