#include "Scanner.h"
#include <unordered_set>

Scanner::Scanner(std::string source) : source(source)
{
}

bool Scanner::isAtEnd()
{
	return pos >= (int)source.length();
}

bool Scanner::match(char c)
{
	if (c == source[pos]) {
		advance();
		return true;
	}
	return false;
}

char Scanner::advance()
{
	if (source[pos] == '\n') line++;
	return source[pos++];
}

char Scanner::peek()
{
	return source[pos];
}

std::string Scanner::currentText()
{
	return source.substr(start, pos - start);
}

Token Scanner::addToken(TokenTypes type)
{
	auto token = Token{ type, start, currentText(), line };
	tokens.push_back(token);
	return token;
}

Token Scanner::addToken(TokenTypes type, std::string text)
{
	auto token = Token{ type, start, text, line };
	tokens.push_back(token);
	return token;
}

Token Scanner::parseNumber(char c)
{
	bool blank = true;
	if (c != '.')
	{
		blank = false;
		while (isdigit(peek()))
			blank = false, advance();
	}
	TokenTypes type = TOKEN_INTEGER;
	if (c == '.' || match('.'))
	{
		type = TOKEN_FLOAT;
		while (isdigit(peek())) blank = false, advance();
	}
	if (blank) type = TOKEN_DOT;
	return addToken(type);
}

Token Scanner::parseString()
{
	advance();//match('"');

	while (peek() != '"' && !isAtEnd()) {
		//if (peek() == '\n') line++;
		if (peek() == '\\')
			advance();
		advance();
	}

	if (isAtEnd()) {
		return Token{TOKEN_ERROR, pos, "Unterminated string."};
	}
	
	advance();
	
	return addToken(TOKEN_STRING, source.substr(start + 1, pos - start - 2));
}

std::unordered_set<std::string> keywords = {
	"string", "print",

	"attribute", "const", "uniform", "varying", "centroid", "break",
	"continue", "do", "for", "while", "if", "else", "in", "out",
	"inout", "float", "int", "void", "bool", "true", "false",
	"invariant", "discard", "return", "mat2", "mat3", "mat4",
	"mat2x2", "mat2x3", "mat2x4", "mat3x2", "mat3x3", "mat3x4",
	"mat4x2", "mat4x3", "mat4x4", "vec2", "vec3", "vec4", "ivec2",
	"ivec3", "ivec4", "bvec2", "bvec3", "bvec4", "sampler1D",
	"sampler2D", "sampler3D", "samplerCube", "sampler1DShadow",
	"sampler2DShadow", "struct",

	"asm", "class", "union", "enum", "typedef", "template", "this",
	"packed", "goto", "switch", "default", "inline", "noinline",
	"volatile", "public", "static", "extern", "external",
	"interface", "long", "short", "double", "half", "fixed",
	"unsigned", "lowp", "mediump", "highp", "precision", "input",
	"output", "hvec2", "hvec3", "hvec4", "dvec2", "dvec3", "dvec4",
	"fvec2", "fvec3", "fvec4", "sampler2DRect", "sampler3DRect",
	"sampler2DRectShadow", "sizeof", "cast", "namespace", "using"
};

Token Scanner::parseAlphaNum()
{
	while (isalnum(peek()) || peek() == '_') advance();
	TokenTypes type = TOKEN_IDENTIFIER;
	if (keywords.find(currentText()) != keywords.end())
		type = TOKEN_KEYWORD;
	return addToken(type);
}

void Scanner::parseComment(bool multiline)
{
	if (!multiline)
	{
		while (!(match('\r') || match('\n'))) advance();
	}
	else
		while (!(match('*') && match('/'))) advance();
}

Token Scanner::scanToken()
{
	switch (char c = advance())
	{
	case '#': return addToken(TOKEN_HASH);
	case ';': return addToken(TOKEN_SEMICOLON);
	case ':': return addToken(TOKEN_COLON);
	case ',': return addToken(TOKEN_COMMA);
	case '\'':return addToken(TOKEN_SQUOTE);

	case '(': return addToken(TOKEN_LPAREN);
	case ')': return addToken(TOKEN_RPAREN);
	case '{': return addToken(TOKEN_LBRACE);
	case '}': return addToken(TOKEN_RBRACE);
	case '[': return addToken(TOKEN_LBRACKET);
	case '=': return match('=') ? addToken(TOKEN_EQUAL_EQUAL) : addToken(TOKEN_EQUAL);
	case '+': return match('=') ? addToken(TOKEN_PLUS_EQUAL) : addToken(TOKEN_PLUS);
	case '-': return match('=') ? addToken(TOKEN_MINUS_EQUAL) : addToken(TOKEN_MINUS);
	case '*': return match('=') ? addToken(TOKEN_STAR_EQUAL) : addToken(TOKEN_STAR);
	case '!': return match('=') ? addToken(TOKEN_BANG_EQUAL) : addToken(TOKEN_BANG);
	case '<': return match('=') ? addToken(TOKEN_LESS_EQUAL) : addToken(TOKEN_LESS);
	case '>': return match('=') ? addToken(TOKEN_GREATER_EQUAL) : addToken(TOKEN_GREATER);
	case '\n': {
		start = pos;
		return scanToken();
	}
	
	case '"': return parseString();

	case '/': {
		if (match('/')) {
			parseComment(false);
			return scanToken();
		}
		else return match('=') ? addToken(TOKEN_FSLASH_EQUAL) : addToken(TOKEN_FSLASH);
	};
	default: {
		if (isdigit(c) || c == '.')
			return parseNumber(c);
		if (isalpha(c))
			return parseAlphaNum();
	}
	}

	return Token();
}

std::vector<Token> ScanTokens(std::string source)
{
	return Scanner(source).scan();
}

std::vector<Token> Scanner::scan()
{
	if (source.length() == 0) return tokens;

	while (!isAtEnd())
	{
		start = pos;
		Token token = scanToken();
		if (token.type == TOKEN_ERROR) break;
		if (start == pos) break;
	}

	tokens.push_back(Token{ TOKEN_EOF, pos, "" });

	return tokens;
}