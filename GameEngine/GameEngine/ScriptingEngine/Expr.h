#pragma once
#include <variant>
#include "Token.h"
#include "Value.h"

struct Expr
{
	enum Type
	{
		BINARY,
		UNARY,
		LITERAL,
		GROUP,
		VARIABLE,
		ASSIGN,
		CALL,
		SWIZZLE,
		SWIZZLE_ASSIGNMENT
	} type;
};

struct ExprBinary : public Expr
{
	Expr* lhs;
	Token op;
	Expr* rhs;
	ExprBinary(Expr* lhs, Token op, Expr* rhs) : Expr{ BINARY }, lhs(lhs), op(op), rhs(rhs) {};
};

struct ExprUnary : public Expr
{
	Token op;
	Expr* rhs;
	ExprUnary(Token op, Expr* rhs) : Expr{ UNARY }, op(op), rhs(rhs) {};
};

struct ExprLiteral : public Expr
{
	Value lit;
	ExprLiteral(Value lit) : Expr{ LITERAL }, lit(lit) {};
};

struct ExprGrouping : public Expr
{
	Expr* expr;
	ExprGrouping(Expr* expr) : Expr{ GROUP }, expr(expr) {};
};

struct ExprVariable : public Expr
{
	std::string name;
	ExprVariable(std::string name) : Expr{ VARIABLE }, name(name) {};
};

struct ExprAssign : public Expr
{
	Expr* variable;
	Expr* expr;
	ExprAssign(Expr* variable, Expr* expr) : Expr{ ASSIGN }, variable(variable), expr(expr) {};
};

struct ExprCall : public Expr
{
	Expr* callee;
	std::vector<Expr*> arguments;
	ExprCall(Expr* callee, std::vector<Expr*> arguments) : 
		Expr{ CALL },
		callee(callee),
		arguments(arguments)
	{};
};

struct ExprSwizzle : public Expr
{
	Expr* value;
	Token swizzle;
	ExprSwizzle(Expr* value, Token swizzle) :
		Expr{ SWIZZLE },
		value(value),
		swizzle(swizzle)
	{};
};

//struct ExprSwizzleAssign : public Expr
//{
//	ExprSwizzle* var;
//	Expr* expr;
//	ExprSwizzleAssign(ExprSwizzle* var, Expr* expr) : Expr{ SWIZZLE_ASSIGNMENT }, var(var), expr(expr) {};
//};