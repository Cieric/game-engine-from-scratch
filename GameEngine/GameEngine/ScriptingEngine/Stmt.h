#pragma once
#include "Value.h"
#include "Expr.h"

struct Stmt
{
	enum Type {
		TYPE_EXPRESSION,
		TYPE_VAR,
		TYPE_BLOCK,
		TYPE_IF,
		TYPE_WHILE,
		TYPE_FUNCTION,
		TYPE_RETURN
	} type;
};

struct StmtExpression : public Stmt
{
	Expr* expr;
	StmtExpression(Expr* expr) :
		Stmt{ TYPE_EXPRESSION },
		expr(expr)
	{};
};

struct StmtVar : public Stmt
{
	std::string name;
	ValueType type;
	Expr* initializer;
	StmtVar(std::string name, ValueType type, Expr* initializer) :
		Stmt{ TYPE_VAR },
		name(name),
		type(type),
		initializer(initializer)
	{};
};

struct StmtBlock : public Stmt
{
	std::vector<Stmt*> stmts;
	StmtBlock(std::vector<Stmt*> stmts) :
		Stmt{ TYPE_BLOCK },
		stmts(stmts)
	{};
};

struct StmtIf : public Stmt
{
	Expr* condition;
	Stmt* thenBranch;
	Stmt* elseBranch;
	StmtIf(Expr* condition, Stmt* thenBranch, Stmt* elseBranch) :
		Stmt{ TYPE_IF },
		condition(condition),
		thenBranch(thenBranch),
		elseBranch(elseBranch)
	{};
};

struct StmtWhile : public Stmt
{
	Expr* condition;
	Stmt* body;
	StmtWhile(Expr* condition, Stmt* body) :
		Stmt{ TYPE_WHILE },
		condition(condition),
		body(body)
	{};
};

struct StmtFunction : public Stmt
{
	ValueType returnType;
	std::string name;
	std::vector<std::tuple<std::string, ValueType>> parameters;
	Stmt* body;
	StmtFunction(ValueType returnType, std::string name, std::vector<std::tuple<std::string, ValueType>> parameters, Stmt* body) :
		Stmt{ TYPE_FUNCTION },
		returnType(returnType),
		name(name),
		parameters(parameters),
		body(body)
	{}
};

struct StmtReturn : public Stmt
{
	Token keyword;
	Expr* value;
	StmtReturn(Token keyword, Expr* value) :
		Stmt{ TYPE_RETURN },
		keyword(keyword),
		value(value)
	{}
};