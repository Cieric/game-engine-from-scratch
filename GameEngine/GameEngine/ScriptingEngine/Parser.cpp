#include "Parser.h"
#include <fmt/format.h>
#include <unordered_set>

std::vector<Stmt*> Parser::parse() {
    std::vector<Stmt*> statements = std::vector<Stmt*>();
    int lastpos = current-1;
    while (!isAtEnd() && lastpos != current) {
        lastpos = current;
        statements.push_back(declaration());
    }

    return statements;
}

//Expr* Parser::parse()
//{
//	Expr* expr = expression();
//    if (error.set) {
//        return nullptr;
//    }
//    return expr;
//}

bool Parser::checkDeclType()
{
    static const std::unordered_set<std::string> keywords = {
        "string", "float", "int", "void", "bool", "mat2", "mat3", "mat4",
        "mat2x2", "mat2x3", "mat2x4", "mat3x2", "mat3x3", "mat3x4",
        "mat4x2", "mat4x3", "mat4x4", "vec2", "vec3", "vec4", "ivec2",
        "ivec3", "ivec4", "bvec2", "bvec3", "bvec4", "sampler1D",
        "sampler2D", "sampler3D", "samplerCube", "sampler1DShadow",
        "sampler2DShadow", "struct"
    };
    if (check(TOKEN_KEYWORD) && keywords.find(peek().text) != keywords.end())
        return true;
    return false;
}

Stmt* Parser::declaration() {
    if (checkDeclType()) return varDeclaration();
    return statement();
}

Stmt* Parser::varDeclaration() {
    Token type = advance();
    
    const std::unordered_map<std::string, ValueType> Key2Type = {
        { "int", ValueType::TYPE_INT },
        { "float", ValueType::TYPE_FLOAT },
        { "bool", ValueType::TYPE_BOOL },
        { "vec2", ValueType::TYPE_FLOAT_2 },
        { "vec3", ValueType::TYPE_FLOAT_3 },
        { "vec4", ValueType::TYPE_FLOAT_4 },
        { "string", ValueType::TYPE_STRING },
        { "func", ValueType::TYPE_FUNCTION },
        { "void", ValueType::TYPE_ERROR },
    };
    ValueType valueType = Key2Type.find(type.text)->second;

    std::string name = consume(TOKEN_IDENTIFIER, "Expect variable name.").text;

    Expr* initializer = nullptr;
    if (match(TOKEN_EQUAL)) {
        initializer = expression();
    }
    else if (match(TOKEN_LPAREN)) { 
        std::vector<std::tuple<std::string, ValueType>> parameters;
        if (!check(TOKEN_RPAREN)) {
            do {
                if (parameters.size() >= 255) {
                    setError(peek().pos, "Can't have more than 255 parameters.");
                }
                Token varType = consume(TOKEN_KEYWORD, "Expect parameter type.");
                Token varName = consume(TOKEN_IDENTIFIER, "Expect parameter name.");

                if (varType.type == TOKEN_ERROR) {
                    setError(varType.pos, varType.text);
                    return nullptr;
                }

                auto finalType = *Key2Type.find(varType.text);
                parameters.push_back(std::tuple<std::string, ValueType>(varName.text, finalType.second));
            } while (match(TOKEN_COMMA));
        }
        consume(TOKEN_RPAREN, "Expect ')' after parameters.");

        consume(TOKEN_LBRACE, "Expect '{' before {} body.");
        Stmt* body = new StmtBlock(block());
        return new StmtFunction(valueType, name, parameters, body);
    }
    consume(TOKEN_SEMICOLON, "Expect ';' after variable declaration.");

    return new StmtVar(name, valueType, initializer);
}

Stmt* Parser::statement()
{
    if (check(TOKEN_KEYWORD) && peek().text == "for") return (advance(), forStatement());
    if (check(TOKEN_KEYWORD) && peek().text == "if") return (advance(), ifStatement());
    if (check(TOKEN_KEYWORD) && peek().text == "while") return (advance(), whileStatement());
    //if (check(TOKEN_KEYWORD) && peek().text == "print") return (advance(), printStatement());
    if (check(TOKEN_KEYWORD) && peek().text == "return") return (advance(), returnStatement());
    if (match(TOKEN_LBRACE)) return new StmtBlock(block());

    return expressionStatement();
}

Stmt* Parser::forStatement() {
    consume(TOKEN_LPAREN, "Expect '(' after 'for'.");
    Stmt* initializer;
    if (match(TOKEN_SEMICOLON)) {
        initializer = nullptr;
    }
    else if (checkDeclType()) {
        initializer = varDeclaration();
    }
    else {
        initializer = expressionStatement();
    }

    Expr* condition = nullptr;
    if (!check(TOKEN_SEMICOLON)) {
        condition = expression();
    }
    consume(TOKEN_SEMICOLON, "Expect ';' after loop condition.");

    Expr* increment = nullptr;
    if (!check(TOKEN_RPAREN)) {
        increment = expression();
    }
    consume(TOKEN_RPAREN, "Expect ')' after for clauses.");

    Stmt* body = statement();

    if (increment != nullptr) {
        body = new StmtBlock(std::vector<Stmt*>{ body, new StmtExpression(increment) });
    }

    if (condition == nullptr) condition = new ExprLiteral(Value(TYPE_BOOL, true));
    body = new StmtWhile(condition, body);
    
    if (initializer != nullptr) {
        body = new StmtBlock(std::vector<Stmt*>{initializer, body});
    }

    return body;
}

Stmt* Parser::ifStatement() {
    consume(TOKEN_LPAREN, "Expect '(' after 'if'.");
    Expr* condition = expression();
    consume(TOKEN_RPAREN, "Expect ')' after if condition.");

    Stmt* thenBranch = statement();
    Stmt* elseBranch = nullptr;
    if (check(TOKEN_KEYWORD) && peek().text == "else") {
        advance();
        elseBranch = statement();
    }

    return new StmtIf(condition, thenBranch, elseBranch);
}

Stmt* Parser::whileStatement() {
    consume(TOKEN_LPAREN, "Expect '(' after 'while'.");
    Expr* condition = expression();
    consume(TOKEN_RPAREN, "Expect ')' after condition.");
    Stmt* body = statement();

    return new StmtWhile(condition, body);
}

//Stmt* Parser::printStatement() {
//    Expr* value = expression();
//    consume(TOKEN_SEMICOLON, "Expect ';' after value.");
//    return new StmtPrint(value);
//}

Stmt* Parser::returnStatement()
{
    Token keyword = previous();
    Expr* value = nullptr;
    if (!check(TOKEN_SEMICOLON)) {
        value = expression();
    }

    consume(TOKEN_SEMICOLON, "Expect ';' after return value.");
    return new StmtReturn(keyword, value);
}

std::vector<Stmt*> Parser::block() {
    std::vector<Stmt*> statements;

    int last = current;
    while (!check(TOKEN_RBRACE) && !isAtEnd()) {
        statements.push_back(declaration());
        if (error.set) break;
        if (current == last) break;
    }

    consume(TOKEN_RBRACE, "Expect '}' after block.");
    return statements;
}

Stmt* Parser::expressionStatement() {
    Expr* expr = expression();
    consume(TOKEN_SEMICOLON, "Expect ';' after expression.");
    return new StmtExpression(expr);
}

Parser::Error Parser::getError()
{
    return error;
}

Expr* Parser::expression() {
    return assignment();
}

Expr* Parser::assignment() {
    Expr* expr = equality();
    if (error.set) return nullptr;

    if (match(TOKEN_EQUAL)) {
        Token equals = previous();
        Expr* value = assignment();
        if (error.set) return nullptr;

        //std::string name = ((ExprVariable*)expr)->name;
        return new ExprAssign(expr, value);

        //setError(equals.pos, "Invalid assignment target.");
    }

    return expr;
}

Expr* Parser::equality()
{
    Expr* expr = comparison();
    if (error.set) return nullptr;

    while (match(TOKEN_BANG_EQUAL, TOKEN_EQUAL_EQUAL)) {
        Token op = previous();
        Expr* right = comparison();
        if (error.set) return nullptr;
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::comparison()
{
    Expr* expr = term();

    while (match(TOKEN_GREATER, TOKEN_GREATER_EQUAL, TOKEN_LESS, TOKEN_LESS_EQUAL)) {
        Token op = previous();
        Expr* right = term();
        if (error.set) return nullptr;
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::term()
{
    Expr* expr = factor();
    if (error.set) return nullptr;

    while (match(TOKEN_MINUS, TOKEN_PLUS)) {
        Token op = previous();
        Expr* right = factor();
        if (error.set) return nullptr;
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::factor()
{
    Expr* expr = unary();
    if (error.set) return nullptr;

    while (match(TOKEN_FSLASH, TOKEN_STAR)) {
        Token op = previous();
        Expr* right = unary();
        if (error.set) return nullptr;
        expr = new ExprBinary(expr, op, right);
    }

    return expr;
}

Expr* Parser::unary()
{
    if (match(TOKEN_BANG, TOKEN_MINUS, TOKEN_PLUS)) {
        Token op = previous();
        Expr* right = unary();
        if (error.set) return nullptr;
        return new ExprUnary(op, right);
    }

    Expr* expr = call();
    if (error.set) return nullptr;
    return expr;
}

Expr* Parser::call() {
    Expr* expr = match(TOKEN_KEYWORD)?new ExprVariable(previous().text): swizzle();
    if (error.set) return nullptr;

    while (true) {
        if (match(TOKEN_LPAREN)) {
            expr = finishCall(expr);
        }
        else {
            break;
        }
    }

    return expr;
}

Expr* Parser::finishCall(Expr* callee) {
    std::vector<Expr*> arguments;
    if (!check(TOKEN_RPAREN)) {
        do {
            arguments.push_back(expression());
        } while (match(TOKEN_COMMA));
    }

    consume(TOKEN_RPAREN, "Expect ')' after arguments.");

    return new ExprCall(callee, arguments);
}

Expr* Parser::swizzle()
{
    Expr* expr = primary();

    while (match(TOKEN_DOT)) {
        consume(TOKEN_IDENTIFIER, "Expected an identifier for swizzle.");
        expr = new ExprSwizzle(expr, previous());
    }

    return expr;
}

Expr* Parser::primary()
{
    if (check(TOKEN_KEYWORD) && peek().text == "false") return (advance(), new ExprLiteral(Value(TYPE_BOOL, false)));
    if (check(TOKEN_KEYWORD) && peek().text == "true") return (advance(), new ExprLiteral(Value(TYPE_BOOL, true)));

    if (match(TOKEN_INTEGER)) return new ExprLiteral(Value(TYPE_INT, stoll(previous().text)));
    if (match(TOKEN_FLOAT)) return new ExprLiteral(Value(TYPE_FLOAT, stod(previous().text)));
    if (match(TOKEN_STRING)) return new ExprLiteral(Value(TYPE_STRING, previous().text));
    
    if (match(TOKEN_IDENTIFIER)) return new ExprVariable(previous().text);

    if (match(TOKEN_LPAREN)) {
        Expr* expr = expression();
        if (error.set) return nullptr;
        consume(TOKEN_RPAREN, "Expect ')' after expression.");
        return new ExprGrouping(expr);
    }

    setError(current, fmt::format("Expected variable or literal but got ({})", peek().text));
    return nullptr;
}

bool Parser::check(TokenTypes type)
{
    if (isAtEnd()) return false;
    return peek().type == type;
}

Token Parser::advance() {
    if (!isAtEnd()) current++;
    return previous();
}

bool Parser::isAtEnd() {
    return peek().type == TOKEN_EOF;
}

Token Parser::peek() {
    return tokens[current];
}

Token Parser::previous() {
    return tokens[current-1];
}

Token Parser::consume(TokenTypes type, std::string message)
{
    if (check(type)) return advance();
    setError(peek().pos, message);
    return Token{ TOKEN_ERROR, current, message };
}

void Parser::setError(int pos, std::string message)
{
    hadError = true;
    error.set = true;
    error.pos = pos;
    error.line = tokens[current].line;
    error.message = message;
}

void Parser::synchronize()
{

}