#include "Value.h"
#include <string>

std::string to_string(Value value)
{
	switch (value.type)
	{
	case TYPE_INT: return std::to_string(value.getData<int64_t>());
	case TYPE_FLOAT: return std::to_string(value.getData<double>());
	case TYPE_BOOL: return std::to_string(value.getData<bool>());
	case TYPE_STRING: return value.getData<std::string>();
	case TYPE_FUNCTION: return value.getData<Callable>().func == nullptr ? "Native Function" : "Unknown Function";
	case TYPE_ERROR: return std::string("Error: ") + std::string(value.getData<Error_t>().msg);
	}

	return "Error: Unknown value type!";
}

std::string to_string(const ValueType value)
{
	switch (value)
	{
	case TYPE_INT: return "int";
	case TYPE_FLOAT: return "float";
	case TYPE_BOOL: return "bool";
	case TYPE_STRING: return "string";
	case TYPE_FUNCTION: return "func";
	case TYPE_ERROR: return "error";
	}

	return "Error: Unknown value type!";
}