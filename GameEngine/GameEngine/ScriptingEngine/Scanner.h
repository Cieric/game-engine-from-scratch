#pragma once
#include <vector>
#include <string>
#include "Token.h"

struct Scanner
{
	std::string source;
	int pos = 0, start = -1;
	std::vector<Token> tokens;
	int line = 1;

	Scanner(std::string source);

	bool isAtEnd();
	bool match(char c);
	char advance();
	char peek();
	std::string currentText();

	Token addToken(TokenTypes type);
	Token addToken(TokenTypes type, std::string text);

	Token parseNumber(char c);
	Token parseString();
	Token parseAlphaNum();
	void parseComment(bool multiline);

	Token scanToken();
	std::vector<Token> scan();
};