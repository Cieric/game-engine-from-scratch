#include "Interpreter.h"
#include "../Objects/Components/ComponentScript.h"
#include <GLFW/glfw3.h>
#include "../Math.h"
#include "../Objects/ObjectBase.h"
#include "fmt/format.h"

extern GLFWwindow* mWindow;

int getKeyFromName(std::string name)
{
	const std::unordered_map<std::string, int> keyMap = {
		{"q", GLFW_KEY_Q},
		{"w", GLFW_KEY_W},
		{"e", GLFW_KEY_E},
		{"a", GLFW_KEY_A},
		{"s", GLFW_KEY_S},
		{"d", GLFW_KEY_D},
		{"space", GLFW_KEY_SPACE},
		{"lshift", GLFW_KEY_LEFT_SHIFT},
	};
	auto iter = keyMap.find(name);
	if (iter == keyMap.end()) return GLFW_KEY_UNKNOWN;
	return iter->second;
}

Interpreter::Interpreter() : returnValue(Value(TYPE_ERROR, Error_t( "return value never set!" )))
{
	env->defineFunction("cos", Callable{ 1, nullptr,
			[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
				return Value(TYPE_FLOAT, std::cos(matchType(TYPE_FLOAT, arguments[0]).getData<double>()));
			}
		});
	env->defineFunction("sin", Callable{ 1, nullptr,
			[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
				return Value(TYPE_FLOAT, std::sin(matchType(TYPE_FLOAT, arguments[0]).getData<double>()));
			}
		});
	env->defineFunction("sqrt", Callable{ 1, nullptr,
			[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
				return Value(TYPE_FLOAT, std::sqrt(matchType(TYPE_FLOAT, arguments[0]).getData<double>()));
			}
		});
	env->defineFunction("getKeyDown", Callable{ 1, nullptr,
			[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
				int key = getKeyFromName(arguments[0].getData<std::string>());
				return Value(TYPE_BOOL, glfwGetKey(mWindow, key) == GLFW_PRESS);
			}
		});
	env->defineFunction("setPos", Callable{ 3, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			auto& mat = ((ComponentScript*)interpreter->getData())->parent->getTransform();
			mat.data[3][0] = (float)arguments[0].getData<double>();
			mat.data[3][1] = (float)arguments[1].getData<double>();
			mat.data[3][2] = (float)arguments[2].getData<double>();
			return Value(TYPE_ERROR, Error_t("function 'setPos' has no return value!"));
		}
		});
	env->defineFunction("getPos", Callable{ 0, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			auto& mat = ((ComponentScript*)interpreter->getData())->parent->getTransform();
			return Value(TYPE_FLOAT_4, mat[3]);
		}
		});
	env->defineFunction("getMousePos", Callable{ 0, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			double mx, my;
			glfwGetCursorPos(mWindow, &mx, &my);
			return Value(TYPE_FLOAT_2, Vec2d{mx, my});
		}
		});
	env->defineFunction("getMouseButton", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			int button = (int)arguments[0].getData<int64_t>();
			auto val = glfwGetMouseButton(mWindow, button);
			return Value(TYPE_BOOL, val == GLFW_PRESS);
		}
		});
	env->defineFunction("setCursorVisible", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			bool visible = (bool)arguments[0].getData<bool>();
			glfwSetInputMode(mWindow, GLFW_CURSOR, visible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
			return Value(TYPE_ERROR, Error_t("function 'print' has no return value!"));
		}
		});
	env->defineFunction("vec3", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			double val = arguments[0].getData<double>();
			return Value(TYPE_FLOAT_3, Vec3d{ val, val, val });
		}
		});
	env->defineFunction("vec2", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			double val = arguments[0].getData<double>();
			return Value(TYPE_FLOAT_2, Vec3d{ val, val, val });
		}
		});
	env->defineFunction("vec2", Callable{ 3, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			double val0 = arguments[0].getData<double>();
			double val1 = arguments[1].getData<double>();
			double val2 = arguments[2].getData<double>();
			return Value(TYPE_FLOAT_2, Vec3d{ val0, val1, val2 });
		}
		});
	env->defineFunction("normalize", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			Vec3d val = arguments[0].getData<Vec3d>();
			return Value(TYPE_FLOAT_3, normalize(val));
		}
		});
	env->defineFunction("cross", Callable{ 2, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			Vec3d lhs = arguments[0].getData<Vec3d>();
			Vec3d rhs = arguments[1].getData<Vec3d>();
			return Value(TYPE_FLOAT_3, cross(lhs, rhs));
		}
		});
	env->defineFunction("lookAt", Callable{ 3, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			Vec3d eye = arguments[0].getData<Vec3d>();
			Vec3d center = arguments[1].getData<Vec3d>();
			Vec3d up = arguments[2].getData<Vec3d>();
			return Value(TYPE_FLOAT_4_4, lookAt(eye, center, up));
		}
		});
	env->defineFunction("setTransform", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			Mat4d val = arguments[0].getData<Mat4d>();
			auto& mat = ((ComponentScript*)interpreter->getData())->parent->getTransform();
			for(int y=0; y<4; y++)
				for(int x=0; x<4; x++)
					mat[y][x] = (float)val[y][x];
			return Value(TYPE_ERROR, Error_t("function 'setTransform' has no return value!"));
		}
		});
	env->defineFunction("print", Callable{ 1, nullptr,
		[](Interpreter* interpreter, Callable* func, std::vector<Value> arguments)->Value {
			auto value = arguments[0];
			switch (value.type)
			{
				case TYPE_INT: fmt::print("{}\n", value.getData<int64_t>()); break;
				case TYPE_FLOAT: fmt::print("{}\n", value.getData<double>()); break;
				case TYPE_FLOAT_2: fmt::print("{{{},{}}}\n", value.getData<Vec3d>().x, value.getData<Vec3d>().y); break;
				case TYPE_FLOAT_3: fmt::print("{{{},{},{}}}\n", value.getData<Vec3d>().x, value.getData<Vec3d>().y, value.getData<Vec3d>().z); break;
				case TYPE_FLOAT_4: fmt::print("{{{},{},{},{}}}\n", value.getData<Vec4d>().x, value.getData<Vec4d>().y, value.getData<Vec4d>().z, value.getData<Vec4d>().w); break;
				case TYPE_BOOL: fmt::print("{}\n", value.getData<bool>()); break;
				case TYPE_STRING: fmt::print("{}\n", value.getData<std::string>()); break;
				case TYPE_ERROR: fmt::print(fg(fmt::color::crimson), "{}\n", value.getData<Error_t>().msg); break;
				default: fmt::print("Unknown type passed to print ({})\n", value.type); break;
			}

			return Value(TYPE_ERROR, Error_t("function 'print' has no return value!"));
		}
		});
}

void Interpreter::interpret(std::vector<Stmt*> statements) {
	for (Stmt* statement : statements) {
		execute(statement);
	}
}

void Interpreter::pushEnv()
{
	env = new Environment(env);
}

void Interpreter::popEnv()
{
	Environment* old = env;
	env = env->enclosing;
	delete old;
}

Environment* Interpreter::getEnv()
{
	return env;
}

void Interpreter::execute(Stmt* stmt)
{
	switch (stmt->type)
	{
	case Stmt::TYPE_EXPRESSION: return visitExpressionStmt((StmtExpression*)stmt);
	case Stmt::TYPE_VAR: return visitVariableStmt((StmtVar*)stmt);
	case Stmt::TYPE_BLOCK: return visitBlockStmt((StmtBlock*)stmt);
	case Stmt::TYPE_IF: return visitIfStmt((StmtIf*)stmt);
	case Stmt::TYPE_WHILE: return visitWhileStmt((StmtWhile*)stmt);
	case Stmt::TYPE_FUNCTION: return visitFunctionStmt((StmtFunction*)stmt);
	case Stmt::TYPE_RETURN: return visitReturnStmt((StmtReturn*)stmt);
	}
}

void Interpreter::visitExpressionStmt(StmtExpression* stmt)
{
	evaluate(stmt->expr);
}

void Interpreter::visitVariableStmt(StmtVar* stmt) {
	Value value = Value(TYPE_ERROR, Error_t( "This value is undefined!" ));
	if (stmt->initializer != nullptr) {
		value = matchType(stmt->type, evaluate(stmt->initializer));
	}
	env->define(stmt->name, stmt->type, value);
}

std::string GetVariableName(Expr* expr)
{
	switch (expr->type)
	{
	case Expr::VARIABLE: return ((ExprVariable*)expr)->name;
	case Expr::SWIZZLE: return GetVariableName(((ExprSwizzle*)expr)->value);
	}
	return "~";
}

struct SwizComp { uint8_t count; int8_t compIdx[4]; };
SwizComp swizzleToArray(std::string swiz)
{
	SwizComp arr = { 0, -1, -1, -1, -1 };
	if (swiz.size() >= 4) return arr;
	arr.count = (uint8_t)swiz.size();
	for (int i = 0; i < swiz.size(); i++) {
		char c = swiz[i];
		switch (c)
		{
		case 's': arr.compIdx[i] = 0; break;
		case 't': arr.compIdx[i] = 1; break;
		case 'u': arr.compIdx[i] = 2; break;
		case 'v': arr.compIdx[i] = 3; break;
		case 'x': arr.compIdx[i] = 0; break;
		case 'y': arr.compIdx[i] = 1; break;
		case 'z': arr.compIdx[i] = 2; break;
		case 'w': arr.compIdx[i] = 3; break;
		case 'r': arr.compIdx[i] = 0; break;
		case 'g': arr.compIdx[i] = 1; break;
		case 'b': arr.compIdx[i] = 2; break;
		case 'a': arr.compIdx[i] = 3; break;
		}
	}
	return arr;
}

Value CalcSwizzle(Value value, SwizComp swiz)
{
	Vec4d _internalValue = value.getData<Vec4d>();
	Vec4d _retValue = Vec4d{ 0.0, 0.0, 0.0, 0.0 };

	for (int i = 0; i < swiz.count; i++)
		_retValue[i] = _internalValue[swiz.compIdx[i]];

	switch (swiz.count)
	{
	case 1: return Value(TYPE_FLOAT, _retValue);
	case 2: return Value(TYPE_FLOAT_2, _retValue);
	case 3: return Value(TYPE_FLOAT_3, _retValue);
	case 4: return Value(TYPE_FLOAT_4, _retValue);
	}

	return Value(TYPE_ERROR, Error_t("Swizzle Error: How the actual fuck!?"));
}

Value ApplySwizzle(Value base, Value value, SwizComp swiz)
{
	Vec4d _internalValue = value.getData<Vec4d>();
	Vec4d _retValue = base.getData<Vec4d>();

	for (int i = 0; i < swiz.count; i++)
		_retValue[swiz.compIdx[i]] = _internalValue[i];

	switch (base.type)
	{
	case TYPE_FLOAT: return Value(TYPE_FLOAT, _retValue);
	case TYPE_FLOAT_2: return Value(TYPE_FLOAT_2, _retValue);
	case TYPE_FLOAT_3: return Value(TYPE_FLOAT_3, _retValue);
	case TYPE_FLOAT_4: return Value(TYPE_FLOAT_4, _retValue);
	}

	return Value(TYPE_ERROR, Error_t("Swizzle Error: How the actual fuck!?"));
}

SwizComp GetSwizzleComponents(Expr* expr)
{
	switch (expr->type)
	{
	case Expr::SWIZZLE: return swizzleToArray(((ExprSwizzle*)expr)->swizzle.text);
	}

	return SwizComp{ 0, {} };
}

Value Interpreter::visitAssignExpr(ExprAssign* expr)
{
	Value value = evaluate(expr->expr);
	Expr* variable = expr->variable;
	switch (expr->variable->type)
	{
		case Expr::VARIABLE: return env->assign(((ExprVariable*)variable)->name, value);
		case Expr::SWIZZLE: {
			std::string name = GetVariableName(variable);
			SwizComp swiz = GetSwizzleComponents(variable);
			Value base = env->get(name);
			base = ApplySwizzle(base, value, swiz);
			env->assign(name, base);
			return value;
		} break;
	}

	
	return Value(TYPE_ERROR, Error_t( "Invalid Assignment!" ));
}

void Interpreter::visitBlockStmt(StmtBlock* stmt)
{
	executeBlock(stmt->stmts);
}

void Interpreter::visitIfStmt(StmtIf* stmt)
{
	if (isTruthy(evaluate(stmt->condition))) {
		execute(stmt->thenBranch);
	}
	else if (stmt->elseBranch != nullptr) {
		execute(stmt->elseBranch);
	}
}

void Interpreter::visitWhileStmt(StmtWhile* stmt)
{
	while (isTruthy(evaluate(stmt->condition))) {
		execute(stmt->body);
	}
}

Value Interpreter::call(Interpreter* interpreter, Callable* func, std::vector<Value> arguments)
{
	interpreter->pushEnv();
	for (int i = 0; i < arguments.size(); i++)
		interpreter->env->define(std::get<0>(func->func->parameters[i]), arguments[i].type, arguments[i]);
	interpreter->visitBlockStmt((StmtBlock*)func->func->body);
	interpreter->returnFromFunc = false;
	Value ret = interpreter->returnValue;
	//interpreter->returnValue = Value();
	interpreter->popEnv();
	return ret;
}

void Interpreter::visitFunctionStmt(StmtFunction* stmt)
{
	Callable function{
		(int)stmt->parameters.size(),
		stmt,
		call
	};
	env->defineFunction(stmt->name, function);
}

void Interpreter::executeBlock(std::vector<Stmt*> statements)
{
	pushEnv();
	for (Stmt* statement : statements) {
		if (returnFromFunc) break;
		execute(statement);
	}
	popEnv();
}

void Interpreter::visitReturnStmt(StmtReturn* stmt)
{
	returnValue = evaluate(stmt->value);
	returnFromFunc = true;
	return;
}

Value Interpreter::evaluate(Expr* expr)
{
	switch (expr->type)
	{
	case Expr::LITERAL: return visitLiteralExpr((ExprLiteral*)expr);
	case Expr::UNARY: return visitUnaryExpr((ExprUnary*)expr);
	case Expr::BINARY: return visitBinaryExpr((ExprBinary*)expr);
	case Expr::GROUP: return visitGroupingExpr((ExprGrouping*)expr);
	case Expr::VARIABLE: return visitVariableExpr((ExprVariable*)expr);
	case Expr::ASSIGN: return visitAssignExpr((ExprAssign*)expr);
	case Expr::CALL: return visitCallExpr((ExprCall*)expr);
	case Expr::SWIZZLE: return visitSwizzleExpr((ExprSwizzle*)expr);
	}

	return Value( TYPE_ERROR, Error_t("Failed to parse Expression (How the hell did you do this!?)") );
}

Value Interpreter::visitLiteralExpr(ExprLiteral* expr)
{
	return expr->lit;
}

Value Interpreter::visitGroupingExpr(ExprGrouping* expr)
{
	return evaluate(expr->expr);
}

Value Interpreter::visitUnaryExpr(ExprUnary* expr)
{
	Value right = evaluate(expr->rhs);

	if (right.type == TYPE_ERROR) return right;
	switch (expr->op.type) {
	case TokenTypes::TOKEN_MINUS: {
		switch (right.type)
		{
		case TYPE_INT: return Value(right.type, -right.getData<int64_t>());
		case TYPE_FLOAT: return Value(right.type, -right.getData<double>());
		}
	}
	case TokenTypes::TOKEN_PLUS: return right;
	}

	return Value(TYPE_ERROR, Error_t( "unknown unary operator" ));
}

Value Interpreter::visitVariableExpr(ExprVariable* expr)
{
	return env->get(expr->name);
}

Value Interpreter::visitCallExpr(ExprCall* expr)
{
	Value callee =
		(expr->callee->type == Expr::VARIABLE) ?
		env->getFunction(((ExprVariable*)expr->callee)->name, expr->arguments.size()) :
		evaluate(expr->callee);

	std::vector<Value> arguments;
	for (Expr* argument : expr->arguments) {
		arguments.push_back(evaluate(argument));
	}

	if (callee.type == TYPE_ERROR)
	{
		return Value(TYPE_ERROR, Error_t(fmt::format("Function call: ERROR {}\n", callee.getData<Error_t>().msg)));
	}

	Callable function = callee.getData<Callable>();
	if (arguments.size() != function.arity) {
		return Value(TYPE_ERROR, Error_t(fmt::format("Expected {} arguments but got {}.", function.arity, arguments.size())) );
	}

	return function.call(this, &function, arguments);
}

Value Interpreter::visitSwizzleExpr(ExprSwizzle* expr)
{
	Value value = evaluate(expr->value);
	if (value.type == TYPE_ERROR) return value;
	SwizComp swiz = GetSwizzleComponents(expr);
	return CalcSwizzle(value, swiz);
}

Value addHelper(Value n1, Value n2)
{
	Value d(TYPE_ERROR, Error_t("Fuck."));
	switch (n1.type) {
	case TYPE_INT: d = Value(TYPE_INT, (int64_t)(n1.getData<int64_t>() + matchType(TYPE_INT, n2).getData<int64_t>())); break;
	case TYPE_FLOAT: d = Value(TYPE_FLOAT, (double)(n1.getData<double>() + matchType(TYPE_FLOAT, n2).getData<double>())); break;
	case TYPE_FLOAT_3: d = Value(TYPE_FLOAT_3, (Vec3d)(n1.getData<Vec3d>() + matchType(TYPE_FLOAT_3, n2).getData<Vec3d>())); break;
	default: d = Value(TYPE_ERROR, Error_t("Failed to add the 2 values!")); break;
	}
	return d;
}

Value subHelper(Value n1, Value n2)
{
	Value d(TYPE_ERROR, Error_t("Fuck."));
	switch (n1.type) {
	case TYPE_INT: d = Value(TYPE_INT, (int64_t)(n1.getData<int64_t>() - matchType(TYPE_INT, n2).getData<int64_t>())); break;
	case TYPE_FLOAT: d = Value(TYPE_FLOAT, (double)(n1.getData<double>() - matchType(TYPE_FLOAT, n2).getData<double>())); break;
	case TYPE_FLOAT_2: d = Value(TYPE_FLOAT_2, (Vec2d)(n1.getData<Vec2d>() - matchType(TYPE_FLOAT_2, n2).getData<Vec2d>())); break;
	default: d = Value(TYPE_ERROR, Error_t("Failed to sub the 2 values!")); break;
	}
	return d;
}

Value multHelper(Value n1, Value n2)
{
	Value d(TYPE_ERROR, Error_t("Fuck."));
	switch (n1.type) {
	case TYPE_INT: d = Value(TYPE_INT, (int64_t)(n1.getData<int64_t>() * matchType(TYPE_INT, n2).getData<int64_t>())); break;
	case TYPE_FLOAT: d = Value(TYPE_FLOAT, (double)(n1.getData<double>() * matchType(TYPE_FLOAT, n2).getData<double>())); break;
	case TYPE_FLOAT_2: d = Value(TYPE_FLOAT_2, (Vec2d)(n1.getData<Vec2d>() * matchType(TYPE_FLOAT_2, n2).getData<Vec2d>())); break;
	case TYPE_FLOAT_3: d = Value(TYPE_FLOAT_3, (Vec3d)(n1.getData<Vec3d>() * matchType(TYPE_FLOAT_3, n2).getData<Vec3d>())); break;
	default: d = Value(TYPE_ERROR, Error_t("Failed to mult the 2 values!")); break;
	}
	return d;
}

Value divHelper(Value n1, Value n2)
{
	Value d(TYPE_ERROR, Error_t("Fuck."));
	switch (n1.type) {
	case TYPE_INT: d = Value(TYPE_INT, (int64_t)(n1.getData<int64_t>() / matchType(TYPE_INT, n2).getData<int64_t>())); break;
	case TYPE_FLOAT: d = Value(TYPE_FLOAT, (double)(n1.getData<double>() / matchType(TYPE_FLOAT, n2).getData<double>())); break;
	default: d = Value(TYPE_ERROR, Error_t("Failed to div the 2 values!")); break;
	}
	return d;
}

Value greaterHelper(Value n1, Value n2)
{
	Value d(TYPE_ERROR, Error_t("Fuck."));
	switch (n1.type) {
	case TYPE_INT: d = Value(TYPE_BOOL, (bool)(n1.getData<int64_t>() > matchType(TYPE_INT, n2).getData<int64_t>())); break;
	case TYPE_FLOAT: d = Value(TYPE_BOOL, (bool)(n1.getData<double>() > matchType(TYPE_FLOAT, n2).getData<double>())); break;
	default: d = Value(TYPE_ERROR, Error_t("Failed to greater the 2 values!")); break;
	}
	return d;
}

Value lessHelper(Value n1, Value n2)
{
	Value d(TYPE_ERROR, Error_t("Fuck."));
	switch (n1.type) {
	case TYPE_INT: d = Value(TYPE_BOOL, (bool)(n1.getData<int64_t>() < matchType(TYPE_INT, n2).getData<int64_t>())); break;
	case TYPE_FLOAT: d = Value(TYPE_BOOL, (bool)(n1.getData<double>() < matchType(TYPE_FLOAT, n2).getData<double>())); break;
	default: d = Value(TYPE_ERROR, Error_t("Failed to less the 2 values!")); break;
	}
	return d;
}

Value Interpreter::visitBinaryExpr(ExprBinary* expr)
{
	Value left = evaluate(expr->lhs);
	Value right = evaluate(expr->rhs);
	if (right.type == TYPE_ERROR) return right;
	if (left.type == TYPE_ERROR) return left;

	switch (expr->op.type) {

	case TOKEN_PLUS: return addHelper(left, right);
	case TOKEN_MINUS: return subHelper(left, right);
	case TOKEN_STAR: return multHelper(left, right);
	case TOKEN_FSLASH: return divHelper(left, right);

	case TOKEN_GREATER: return greaterHelper(left, right);
	case TOKEN_LESS: return lessHelper(left, right);
	}

	Value ret = Value(TYPE_ERROR, Error_t( fmt::format("unknown binary operator {} for types {} and {}\n", to_string(expr->op.type), left.type, right.type) ));
	return ret;
}

#undef EVAL_OP

bool Interpreter::isTruthy(Value value)
{
	if (value.type == TYPE_ERROR) return false;
	return matchType(TYPE_BOOL, value).getData<bool>();
}

Value Interpreter::CallFunction(std::string functionName, std::vector<Value> args)
{
	Value callee = env->getFunction(functionName, args.size());

	if (callee.type == TYPE_ERROR) return callee;

	Callable function = callee.getData<Callable>();
	if (args.size() != function.arity) {
		return Value(TYPE_ERROR, Error_t( fmt::format("Expected {} arguments but got {}.", function.arity, args.size()) ));
	}

	return function.call(this, &function, args);
}

void Interpreter::setData(void* data)
{
	externalData = data;
}

void* Interpreter::getData()
{
	return externalData;
}