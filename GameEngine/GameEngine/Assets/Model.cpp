#include "Model.h"

#include <iostream>
#include <fmt/format.h>
#include <unordered_set>
#include <stack>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

#pragma region Bind Mesh Code

Mesh::Mesh(std::map<int, GLuint>& vbos, tinygltf::Model& model, tinygltf::Mesh& mesh) {
	for (size_t i = 0; i < model.bufferViews.size(); ++i) {
		const tinygltf::BufferView& bufferView = model.bufferViews[i];
		if (bufferView.target == 0) {
			printf("WARN: bufferView.target is zero\n");
			continue;
		}

		const tinygltf::Buffer& buffer = model.buffers[bufferView.buffer];
		std::cout << "bufferview.target " << bufferView.target << std::endl;

		GLuint vbo;
		glGenBuffers(1, &vbo);
		vbos[(const int)i] = vbo;
		glBindBuffer(bufferView.target, vbo);

		std::cout << "buffer.data.size = " << buffer.data.size()
			<< ", bufferview.byteOffset = " << bufferView.byteOffset
			<< std::endl;

		glBufferData(bufferView.target, bufferView.byteLength,
			&buffer.data.at(0) + bufferView.byteOffset, GL_STATIC_DRAW);
	}

	for (size_t i = 0; i < mesh.primitives.size(); ++i) {
		tinygltf::Primitive& primitive = mesh.primitives[i];

		for (auto& attrib : primitive.attributes) {
			tinygltf::Accessor& accessor = model.accessors[attrib.second];
			int byteStride =
				accessor.ByteStride(model.bufferViews[accessor.bufferView]);
			glBindBuffer(GL_ARRAY_BUFFER, vbos[accessor.bufferView]);

			int size = 1;
			if (accessor.type != TINYGLTF_TYPE_SCALAR) {
				size = accessor.type;
			}

			int vaa = -1;
			if (attrib.first.compare("POSITION") == 0) vaa = 0;
			if (attrib.first.compare("NORMAL") == 0) vaa = 1;
			if (attrib.first.compare("TEXCOORD_0") == 0) vaa = 2;
			if (vaa > -1) {
				glEnableVertexAttribArray(vaa);
				glVertexAttribPointer(vaa, size, accessor.componentType,
					accessor.normalized ? GL_TRUE : GL_FALSE,
					byteStride, BUFFER_OFFSET(accessor.byteOffset));
			}
			else
				std::cout << "vaa missing: " << attrib.first << std::endl;
		}

		if (primitive.indices != -1)
		{
			tinygltf::Accessor& indexAccessor = model.accessors[primitive.indices];
			prims.push_back(Primative{
				primitive.mode,
				true,
				indexAccessor.count,
				indexAccessor.componentType,
				indexAccessor.byteOffset
				});
		}
		else
		{
			tinygltf::Accessor& posAccessor = model.accessors[0];
			prims.push_back(Primative{
				primitive.mode,
				false,
				posAccessor.count,
				posAccessor.componentType,
				posAccessor.byteOffset
				});
		}
	}

	if (model.textures.size() > 0) {
		// fixme: Use material's baseColor
		tinygltf::Texture& tex = model.textures[0];

		if (tex.source > -1) {

			glGenTextures(1, &texid);

			tinygltf::Image& image = model.images[tex.source];

			glBindTexture(GL_TEXTURE_2D, texid);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			GLenum format = GL_RGBA;

			if (image.component == 1) {
				format = GL_RED;
			}
			else if (image.component == 2) {
				format = GL_RG;
			}
			else if (image.component == 3) {
				format = GL_RGB;
			}
			else {
				// ???
			}

			GLenum type = GL_UNSIGNED_BYTE;
			if (image.bits == 8) {
				// ok
			}
			else if (image.bits == 16) {
				type = GL_UNSIGNED_SHORT;
			}
			else {
				// ???
			}

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, format, type, &image.image.at(0));
		}
	}
}

void Model::bindModel(tinygltf::Model& model) {
	std::map<int, GLuint> vbos;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	const tinygltf::Scene& scene = model.scenes[model.defaultScene];

	std::stack<tinygltf::Node> nodes;

	for (size_t i = 0; i < scene.nodes.size(); ++i) {
		nodes.push(model.nodes[scene.nodes[i]]);
	}

	while (nodes.size() > 0)
	{
		const auto& node = nodes.top();
		nodes.pop();
		if ((node.mesh >= 0) && (node.mesh < model.meshes.size())) {
			meshes.push_back(Mesh(vbos, model, model.meshes[node.mesh]));
		}

		for (size_t i = 0; i < node.children.size(); i++) {
			assert((node.children[i] >= 0) && (node.children[i] < model.nodes.size()));
			nodes.push(model.nodes[node.children[i]]);
			//auto nMeshes = bindModelNodes(model, model.nodes[node.children[i]]);
			//meshes.insert(meshes.begin(), nMeshes.begin(), nMeshes.end());
		}
	}

	glBindVertexArray(0);

	for (size_t i = 0; i < vbos.size(); ++i) {
		glDeleteBuffers(1, &vbos[(const int)i]);
	}
}

#pragma endregion

	Model::Model()
	{
	}

	Model::Model(std::filesystem::path path)
	{
		tinygltf::TinyGLTF loader;
		std::string err;
		std::string warn;

		std::string filename = path.generic_string();

		bool res = loader.LoadASCIIFromFile(&model, &err, &warn, filename);
		if (!warn.empty()) {
			printf("WARN: %s\n", warn.c_str());
		}

		if (!err.empty()) {
			printf("ERR: %s\n", err.c_str());
		}

		if (!res)
			fmt::print("Failed to load glTF: {}\n", filename);

		bindModel(model);
	}

	void Model::Draw() const
	{
		glBindVertexArray(vao);
		for (const Mesh& mesh : meshes) {
			mesh.Draw();
		}
		glBindVertexArray(0);
	}

	void Mesh::Draw() const
	{
		glBindTexture(GL_TEXTURE_2D, texid);
		for (const auto& prim : prims) {
			if (prim.indexed)
			{
				glDrawElements(prim.mode, (GLsizei)prim.count,
					prim.componentType,
					BUFFER_OFFSET(prim.byteOffset));
			}
			else
			{
				glDrawArrays(prim.mode, 0, (GLsizei)prim.count);
			}
		}
	}