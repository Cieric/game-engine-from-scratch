#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include "../Loading/tiny_gltf.h"
#include <filesystem>

struct Primative
{
    int mode : 31;
    bool indexed : 1;
    size_t count;
    int componentType;
    size_t byteOffset;
};

class Mesh
{
    GLuint texid;
    std::vector<Primative> prims;
public:
    Mesh(std::map<int, GLuint>& vbos, tinygltf::Model& model, tinygltf::Mesh& mesh);
    void Draw() const;
};

class Model
{
    tinygltf::Model model;
    std::vector<int32_t> renderCommands;
    GLuint vao;
    GLuint texid;
    std::vector<Mesh> meshes;
public:
    Model();
    Model(std::filesystem::path path);

    void Draw() const;

    void bindModel(tinygltf::Model& model);
};