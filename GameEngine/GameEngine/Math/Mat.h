#pragma once
#include <utility>
#include "Vec.h"

template<typename T, size_t rows, size_t cols>
struct Mat
{
	Vec<Vec<T, rows>, cols> data;
	T* operator&()
	{
		return (T*)&data;
	}

	Vec<T, rows>& operator[](size_t col)
	{
		return data[col];
	}

	const Vec<T, rows> operator[](size_t col) const
	{
		return data[col];
	}

	T& operator()(size_t row, size_t col)
	{
		return data[row][col];
	}

	const T operator()(size_t row, size_t col) const
	{
		return data[row][col];
	}

	template<size_t cols2>
	Mat<T,rows,cols> operator*(Mat<T,cols,cols2> lhs)
	{
		Mat<T, rows, cols> res = {};
		for (size_t i = 0; i < rows; ++i)
			for (size_t j = 0; j < cols2; ++j)
				for (size_t k = 0; k < cols; ++k)
					res(i,j) += (*this)(i,k) * lhs(k,j);
		return res;
	}

	Mat<T, rows, cols>& operator=(const Mat<T, rows, cols> rhs)
	{
		for (size_t i = 0; i < cols; i++)
			data[i] = rhs.data[i];
		return *this;
	}
};

template<typename T>
Mat<T, 4, 4> lookAt(Vec<T, 3> const& eye, Vec<T, 3> const& center, Vec<T, 3> const& up)
{
	Vec<T, 3> f = normalize(center - eye);
	Vec<T, 3> u = normalize(up);
	Vec<T, 3> s = normalize(cross(f, u));
	u = cross(s, f);
	f = -f;

	Mat<T, 4, 4> Result = {
		s.x, u.x, f.x, 0.0,
		s.y, u.y, f.y, 0.0,
		s.z, u.z, f.z, 0.0,
		-dot(s, eye), -dot(u, eye), dot(-f, eye), 1.0
	};
	//Result[0][0] = s.x;
	//Result[1][0] = s.y;
	//Result[2][0] = s.z;
	//Result[0][1] = u.x;
	//Result[1][1] = u.y;
	//Result[2][1] = u.z;
	//Result[0][2] = -f.x;
	//Result[1][2] = -f.y;
	//Result[2][2] = -f.z;
	//Result[3][0] = -dot(s, eye);
	//Result[3][1] = -dot(u, eye);
	//Result[3][2] = dot(-f, eye);
	//Result[3][3] = 1;
	return Result;
}

template<typename T>
Mat<T, 4, 4> rotate(Vec<T, 3> r)
{
	T one = (T)1.0;
	T zer = (T)0.0;
	float sa = sin(r.z);
	float sb = sin(r.y);
	float sy = sin(r.x);
	float ca = cos(r.z);
	float cb = cos(r.y);
	float cy = cos(r.x);
	Mat<T, 4, 4> Result = {
		ca*cb, ca*sb*sy-sa*cy, ca*sb*cy+sa*sy, zer,
        sa*cb, sa*sb*sy+ca*cy, sa*sb*cy-ca*sy, zer,
		  -sb,          cb*sy,          cb*cy, zer,
		zer, zer, zer, one,
	};

	return Result;
}