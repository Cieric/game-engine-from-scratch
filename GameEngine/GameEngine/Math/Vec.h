#pragma once
#include <cstddef>

template<typename T, std::size_t D>
struct Vec
{
	union {
		T data[D];
		struct {
			T x, y, z, w;
		};
	};

	Vec<T, D> operator*(const Vec<T, D> rhs) const
	{
		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = data[i] * rhs.data[i];
		return output;
	}

	Vec<T, D> operator/(const Vec<T, D> rhs) const
	{
		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = data[i] / rhs.data[i];
		return output;
	}

	Vec<T, D> operator*(const T rhs) const
	{
		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = data[i] * rhs;
		return output;
	}

	Vec<T, D> operator/(const T rhs) const
	{
		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = data[i] / rhs;
		return output;
	}

	Vec<T, D> operator-(const Vec<T, D> rhs) const
	{
		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = data[i] - rhs[i];
		return output;
	}

	Vec<T, D> operator+(const Vec<T, D> rhs) const
	{
		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = data[i] + rhs[i];
		return output;
	}

	Vec<T, D> operator-() {

		Vec<T, D> output;
		for (size_t i = 0; i < D; i++)
			output[i] = -data[i];
		return output;
	}

	Vec<T, D>& operator=(const Vec<T, D> rhs)
	{
		for (size_t i = 0; i < D; i++)
			data[i] = rhs[i];
		return *this;
	}

	Vec<T, D>& operator+=(const Vec<T, D> rhs)
	{
		for (size_t i = 0; i < D; i++)
			data[i] += rhs[i];
		return *this;
	}

	T& operator[](size_t x)
	{
		return data[x];
	}

	const T& operator[](const size_t x) const
	{
		return data[x];
	}
};

template<typename T, size_t D>
T dot(Vec<T, D> lhs, Vec<T, D> rhs)
{
	T output = T{};
	for (size_t i = 0; i < D; ++i)
		output += lhs[i] * rhs[i];
	return output;
}

template<typename T, size_t D>
T distance(Vec<T, D> input)
{
	return sqrt(dot(input, input));
}

template<typename T, size_t D>
Vec<T, D> normalize(Vec<T, D> input)
{
	return input / distance(input);
}

template<typename T>
Vec<T, 3> cross(Vec<T, 3> a, Vec<T, 3> b)
{
	return Vec<T, 3>{
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x,
	};
}