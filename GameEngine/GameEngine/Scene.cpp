#include "Scene.h"

Scene::~Scene()
{
	objects.clear();
	for (auto key : componentMaps)
		delete (handle_map_null*)key.second;
	componentMaps.clear();
}

Id_T Scene::SpawnObject()
{
	return objects.insert(ObjectBase());
}

ObjectBase& Scene::GetObject(Id_T id)
{
	return objects[id];
}

Id_T Scene::GetObjectByName(std::string name)
{
	for (auto id : objects.getIds())
	{
		if (objects[id].Name == name)
			return id;
	}
	return NullId_T;
}

void Scene::LoadScene(std::filesystem::path sceneFile)
{
	using json = nlohmann::json;
	json sceneSerialized = json::parse(Utils::ReadFile(sceneFile));

	for (auto object : sceneSerialized["objects"])
	{
		Id_T obj = SpawnObject();
		GetObject(obj).getTransform() = object["transform"].get<Mat4f>();
		if(object.find("name") != object.end())
			GetObject(obj).Name = object["name"];
		auto& components = object["components"];
		for (auto it = components.begin(); it != components.end(); ++it)
		{
			auto compID = hash_c_string(it.key().c_str());
			if (_globalComponentConstructors.find(compID) == _globalComponentConstructors.end())
			{
				Utils::PrintError("Component ({}) constructor not found.", it.key());
			}
			_globalComponentConstructors[compID](this, obj, it.value());
		}
	}
}
