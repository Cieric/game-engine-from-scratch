#pragma once
#include <vector>
#include <unordered_map>
#include "Utils/handle_map.h"
#include "Predeclare.h"
#include "Objects/Components/ComponentBase.h"
#include "Objects/ComponentGlobalIDManager.h"
#include "Objects/ObjectBase.h"
#include "Loading/json.hpp"
#include "Math.h"

#ifdef GetObject
#undef GetObject
#endif

using nlohmann::json;

template<typename T, size_t D>
void to_json(json& j, const Vec<T, D>& v) {
	j = json::array();
	for (int i = 0; i < D; i++)
		j += v[i];
}

template<typename T, size_t D>
void from_json(const json& j, Vec<T, D>& v) {
	for(int i=0; i<D; i++)
		j.at(i).get_to(v[i]);
}

template<typename T, size_t rows, size_t cols>
void to_json(json& j, const Mat<T, rows, cols>& m) {
	j = json::array();
	for (int i = 0; i < cols; i++)
		j += m[i];
}

template<typename T, size_t rows, size_t cols>
void from_json(const json& j, Mat<T, rows, cols>& m) {
	for (int i = 0; i < cols; i++)
		j.at(i).get_to(m[i]);
}

class Scene
{
	handle_map<ObjectBase> objects = handle_map<ObjectBase>(0, 100000);
	std::unordered_map<size_t, void*> componentMaps;

public:
	~Scene();
	Id_T SpawnObject();
	ObjectBase& GetObject(Id_T id);
	Id_T GetObjectByName(std::string name);
	std::vector<ObjectBase> GetObjects()
	{
		return objects.getItems();
	}

	template<typename ComponentType>
	handle_map<ComponentType>& GetComponentHandler()
	{
		auto compId = ComponentName<ComponentType>::Get();
		if (componentMaps.find(compId) == componentMaps.end())
			componentMaps[compId] = new handle_map<ComponentType>((uint16_t)componentMaps.size(), 10);
		return *(handle_map<ComponentType>*)componentMaps[compId];
	}

	template<typename ComponentType>
	Id_T AddComponent(Id_T objId, ComponentType& compIdx)
	{
		auto& map = GetComponentHandler<ComponentType>();
		auto& obj = objects[objId];
		compIdx.parent = &obj;
		Id_T id = map.insert(compIdx);
		obj.AddComponent<ComponentType>(id);
		return id;
	}

	void LoadScene(std::filesystem::path sceneFile);
};