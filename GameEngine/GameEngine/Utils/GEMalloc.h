#pragma once
#include <memory>

void MemoryUsageInitialize();
void SetBreakpointOnLeaks();
void PrintMemoryUsage();
void ResetMemoryUsageStats();
void* operator new(size_t size, const char* file, int line);
void operator delete(void* memory, size_t size);
