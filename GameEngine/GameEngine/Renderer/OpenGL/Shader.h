#pragma once
#include "../IShader.h"
#include "../../Math.h"
#include <GL/glew.h>

class Shader : public IShader
{
	GLuint vs;
	GLuint fs;
	handle_map<GLint> binds = handle_map<GLint>(256, 8);
	std::unordered_map<std::string, Id_T> name_binds;
public:
	GLuint program;
	virtual void LoadVertexShader(std::filesystem::path path) override;
	virtual void LoadFragmentShader(std::filesystem::path path) override;
	virtual void Build() override;
	virtual void applyMaterial(Material& mat) override;
	virtual Id_T FindBindPoint(std::string name) override;
	virtual void Use() override;

	virtual bool Bind(Id_T handle, const Mat4f value) override;
	virtual bool Bind(Id_T handle, const float value) override;
	virtual bool Bind(Id_T handle, const double value) override;
	virtual bool Bind(Id_T handle, const int value) override;
	virtual bool Bind(Id_T handle, const Vec3f value) override;
};