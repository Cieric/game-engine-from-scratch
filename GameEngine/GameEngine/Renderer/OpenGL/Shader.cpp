#include "Shader.h"
#include "../../Utils.h"

void Shader::LoadVertexShader(std::filesystem::path path)
{
	const char* vertex_shader_source = Utils::ReadFile(path);
	DEFER(free((void*)vertex_shader_source));
	vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader_source, nullptr);
	glCompileShader(vs);

	int success;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(vs, 512, NULL, infoLog);
		Utils::PrintError("ERROR::SHADER::FRAG::LINKING_FAILED\n{}\n", std::string(infoLog));
	}
}

void Shader::LoadFragmentShader(std::filesystem::path path)
{
	const char* fragment_shader_source = Utils::ReadFile(path);
	DEFER(free((void*)fragment_shader_source));
	fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader_source, nullptr);
	glCompileShader(fs);

	int success;
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog( fs, sizeof(infoLog), NULL, infoLog);
		Utils::PrintError("ERROR::SHADER::FRAG::LINKING_FAILED\n{}\n", std::string(&infoLog[0]));
	}
}

void Shader::Build()
{
	program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glDeleteShader(vs);
	glDeleteShader(fs);
	int success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(program, 512, NULL, infoLog);
		Utils::PrintError("ERROR::SHADER::PROGRAM::LINKING_FAILED\n{}\n", std::string(infoLog));
	}
}

void Shader::applyMaterial(Material& mat)
{
}

Id_T Shader::FindBindPoint(std::string name)
{
	if(name_binds.find(name) == name_binds.end())
	{
		GLint loc = glGetUniformLocation(program, name.c_str());
		if(loc == -1)
			Utils::PrintError("failed to find bind point {}\n", name.c_str());
		Id_T id = binds.insert(loc);
		name_binds.insert_or_assign(name, id);
	}
	return name_binds[name];
}

void Shader::Use()
{
	glUseProgram(program);
}

bool Shader::Bind(Id_T handle, const Mat4f value)
{
	if(binds[handle] == -1) return false;
	glProgramUniformMatrix4fv(program, binds[handle], 1, false, (const GLfloat*)&value);
	return true;
}

bool Shader::Bind(Id_T handle, const float value)
{
	if(binds[handle] == -1) return false;
	glProgramUniform1f(program, binds[handle], value);
	return true;
}

bool Shader::Bind(Id_T handle, const double value)
{
	if(binds[handle] == -1) return false;
	glProgramUniform1d(program, binds[handle], value);
	return true;
}

bool Shader::Bind(Id_T handle, const int value)
{
	if(binds[handle] == -1) return false;
	glProgramUniform1i(program, binds[handle], value);
	return true;
}

bool Shader::Bind(Id_T handle, const Vec3f value)
{
	if(binds[handle] == -1) return false;
	glProgramUniform3f(program, binds[handle], value.x, value.y, value.z);
	return true;
}
