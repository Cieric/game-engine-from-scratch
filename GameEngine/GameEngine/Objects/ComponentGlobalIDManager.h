#pragma once
#include <unordered_map>
#include "../Utils.h"
#include "../Loading/json.hpp"
#include "ComponentName.h"

class Scene;

typedef void(*ConstructComponent)(Scene* scene, Id_T obj, nlohmann::json data);

extern std::unordered_map<size_t, size_t> _globalComponentIds;
extern std::unordered_map<size_t, ConstructComponent> _globalComponentConstructors;

static int _RegisterComponentImpl(const char* compName, ConstructComponent func)
{
	size_t compID = hash_c_string(compName);
	if (_globalComponentIds.find(compID) == _globalComponentIds.end())
	{
		_globalComponentIds[compID] = _globalComponentIds.size();
		_globalComponentConstructors[compID] = func;
	}
	fmt::print("registered {} as {}\n", compName, _globalComponentIds[compID]);
	return 0;
}

#define RegisterComponent(compName) \
template <> \
struct ComponentName<compName> { static const size_t Get() { return hash_c_string(#compName); } }; \
Initialize({ return _RegisterComponentImpl(#compName, compName::Add); })

