#pragma once
#include <string>
#include <typeinfo>

size_t hash_c_string(const char* str);

template <typename T>
struct ComponentName { static const size_t Get() { return hash_c_string(typeid(T).name()); } };