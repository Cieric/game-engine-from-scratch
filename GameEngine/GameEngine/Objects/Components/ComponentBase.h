#pragma once
#include <string>
#include <typeinfo>
#include <assert.h>
#include "../../Predeclare.h"
#include "../ComponentName.h"
#include "../../Utils/handle_map.h"
#include "../../Loading/json.hpp"
#include "../ObjectBase.h"

class Scene;

class ComponentBase
{
public:
	ObjectBase* parent;
	ComponentBase() = default;
};
