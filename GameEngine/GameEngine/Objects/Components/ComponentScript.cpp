#include "ComponentScript.h"
#include "../ComponentGlobalIDManager.h"
#include "../../Scene.h"
#include "../../Loading/LoadingSystem.h"
#include "../../ScriptingEngine/Scanner.h"
#include "../../ScriptingEngine/Parser.h"
#include <vector>

RegisterComponent(ComponentScript);

ComponentScript::ComponentScript()
{
}

ComponentScript::ComponentScript(nlohmann::json data)
{
	path = data.at(0);
	source = Utils::ReadFile(path);
	Scanner scanner = Scanner(source);
	auto tokens = scanner.scan();

	//for (int i = 0; i < tokens.size(); i++)
	//{
	//	auto token = tokens[i];
	//	std::string text = token.text;
	//	fmt::print(fmt::fg(fmt::color::light_blue), "{:>16} ({},{}): {}\n", to_string(token.type), token.line, token.pos, text);
	//}

	Parser parser = Parser(tokens);
	std::vector<Stmt*> stmts = parser.parse();
	
	Parser::Error parserError = parser.getError();
	if (parserError.set == true) {
		Utils::PrintError("Error: line#{} {}\n", parserError.line, parserError.message);
		return;
	}
	interpreter = Interpreter();
	interpreter.interpret(stmts);
	//interpreter.setData(this);
	//interpreter.CallFunction("Initialize", {});
	
	
	//auto value = interpreter.evaluate();

	//__debugbreak();
	//for (auto& token : tokens)
	//{
	//	if (token.type == TOKEN_KEYWORD)
	//		fmt::print(fg(fmt::color::yellow), "{} ", token.text);
	//	else
	//		fmt::print("{} ", token.text);
	//}
}

void ComponentScript::Add(Scene* scene, Id_T obj, nlohmann::json data)
{
	ComponentScript compIdx(data);
	scene->AddComponent(obj, compIdx);
}

static ObjectBase* _this = nullptr;

const std::string str_Update = "Update";
void ComponentScript::Update(float deltaTime)
{
	interpreter.setData(this);
	interpreter.CallFunction(str_Update, { Value(TYPE_FLOAT, (double)deltaTime) });
}