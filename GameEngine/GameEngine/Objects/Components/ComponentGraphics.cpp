#include "ComponentGraphics.h"
#include "../ComponentGlobalIDManager.h"
#include "../../Scene.h"
#include "../../Loading/LoadingSystem.h"

RegisterComponent(ComponentGraphics);

ComponentGraphics::ComponentGraphics()
{
	//printf("%s\n", __FUNCTION__);
}

ComponentGraphics::ComponentGraphics(nlohmann::json data)
{
	std::string path = data.at(0);
	if (Models.find(path) == Models.end())
	{
		Models[path] = Model(path);
		//Models[path].Compile();
	}
	model = &Models[path];
}

void ComponentGraphics::Add(Scene* scene, Id_T obj, nlohmann::json data)
{
	ComponentGraphics compIdx(data);
	scene->AddComponent(obj, compIdx);
}
