#pragma once
#include "ComponentBase.h"
#include "../../Predeclare.h"
//#include "tiny_gltf.h"
#include "../../Assets/Model.h"
#include <GL/glew.h>

class Scene;

class ComponentGraphics : public ComponentBase
{
public:
	Model* model;
	GLuint id;
	ComponentGraphics();
	ComponentGraphics(nlohmann::json data);

	static void Add(Scene* scene, Id_T obj, nlohmann::json data);
};