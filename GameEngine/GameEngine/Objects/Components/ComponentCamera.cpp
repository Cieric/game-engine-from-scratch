#include "ComponentCamera.h"
#include "../ComponentGlobalIDManager.h"
#include "../../Math.h"
#include <GL/glew.h>
#include "../../Scene.h"

RegisterComponent(ComponentCamera);

ComponentCamera::ComponentCamera()
{
}

ComponentCamera::ComponentCamera(nlohmann::json data)
{
}

Mat4f mat_perspective(float angle, float ratio, float near, float far)
{
    float tan_half_angle = tan(angle / 2);

    Mat4f to_return = {};

    to_return(0, 0) = 1 / (ratio * tan_half_angle);
    to_return(1, 1) = 1 / (tan_half_angle);
    to_return(2, 2) = -(far + near) / (far - near);
	to_return(2, 3) = -1;
    to_return(3, 2) = -(2 * far * near) / (far - near);
    return to_return;
}

Mat4f ComponentCamera::GetProjection(int width, int height)
{
    constexpr float near = 0.1f;//std::numeric_limits<float>::epsilon() * 2.0f;
    constexpr float far = std::numeric_limits<float>::max() / 2.0f;
    return mat_perspective(89.0f, (float)width / (float)height, near, far);
}

void ComponentCamera::Add(Scene* scene, Id_T obj, nlohmann::json data)
{
    ComponentCamera compIdx(data);
    scene->AddComponent(obj, compIdx);
}