#pragma once
#include "ComponentBase.h"
#include "../../Math.h"

class ComponentCamera : public ComponentBase
{
	Mat4f projection;
public:
	ComponentCamera();
	ComponentCamera(nlohmann::json data);
	Mat4f GetProjection(int width, int height);

	static void Add(Scene* scene, Id_T obj, nlohmann::json data);
};