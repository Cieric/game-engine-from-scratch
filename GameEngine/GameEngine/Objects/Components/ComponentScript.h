#pragma once
#include "ComponentBase.h"
#include "../../ScriptingEngine/Interpreter.h"

class Scene;

class ComponentScript : public ComponentBase
{
	std::string path;
	std::string source;

	Interpreter interpreter;
public:
	ComponentScript();
	ComponentScript(nlohmann::json data);

	static void Add(Scene* scene, Id_T obj, nlohmann::json data);

	void Update(float deltaTime);
};