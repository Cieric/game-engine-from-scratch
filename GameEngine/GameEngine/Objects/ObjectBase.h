#pragma once
#include "../Math.h"
#include "ComponentGlobalIDManager.h"
#include "ComponentName.h"

class ObjectBase
{
	Mat4f transform = {
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,0,0,1
	};
	std::vector<std::vector<Id_T>> componentIds = {};

public:
	std::string Name;

	ObjectBase();

	template<typename ComponentType>
	void AddComponent(Id_T compIdx)
	{
		std::size_t compId = ComponentName<ComponentType>::Get();
		std::size_t id = _globalComponentIds[compId];
		componentIds[id].push_back(compIdx);
	}

	Mat4f& getTransform();
};