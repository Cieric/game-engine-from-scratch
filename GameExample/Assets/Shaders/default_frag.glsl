#version 430

in vec3 f_position;
in vec2 f_texCoord;
in vec3 f_normal;

struct Material
{
	sampler2D tex_AMBIENT;
	sampler2D tex_AMBIENT_OCCLUSION;
	sampler2D tex_BASE_COLOR;
	sampler2D tex_DIFFUSE;
	sampler2D tex_DIFFUSE2;
	sampler2D tex_DIFFUSE_ROUGHNESS;
	sampler2D tex_DISPLACEMENT;
	sampler2D tex_EMISSION_COLOR;
	sampler2D tex_EMISSIVE;
	sampler2D tex_HEIGHT;
	sampler2D tex_LIGHTMAP;
	sampler2D tex_METALNESS;
	sampler2D tex_NONE;
	sampler2D tex_NORMALS;
	sampler2D tex_NORMAL_CAMERA;
	sampler2D tex_OPACITY;
	sampler2D tex_REFLECTION;
	sampler2D tex_SHININESS;
	sampler2D tex_SPECULAR;
	sampler2D tex_UNKNOWN;
};

uniform Material material;

uniform bool everything = false;
out vec4 frag_color;

uniform vec3 lightPos; 
uniform vec3 viewPos; 
uniform vec3 lightColor;

// All components are in the range [0...1], including hue.
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
	//vec4 color = texture2D(material.tex_DIFFUSE, f_texCoord);
	//float dist = distance(f_position, viewPos) / 10.0f;
	//vec3 color = hsv2rgb(vec3(dist, 1.0, 1.0));
	//frag_color = vec4(color.xyz, 1.0);
	frag_color = vec4(f_normal, 1.0);
}