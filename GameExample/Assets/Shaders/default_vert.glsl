#version 430
layout(location = 0) in vec3 vert;
layout(location = 1) in vec3 norm;
layout(location = 2) in vec2 texCoord;

out vec3 f_position;
out vec2 f_texCoord;
out vec3 f_normal;

layout(location = 0) uniform mat4 model;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 proj;

void main() {
	mat4 mvp = proj * view * model;
	gl_Position = mvp * vec4(vert, 1.0);
	f_position = vec3(model * vec4(vert, 1.0));
	f_texCoord = texCoord;
	f_normal = norm;
}