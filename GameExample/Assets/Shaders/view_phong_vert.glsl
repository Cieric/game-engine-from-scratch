#version 430

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 norm;
layout(location = 2) in vec2 texCoord;

out vec3 f_position;
out vec3 f_normal;
out vec2 f_texCoord;
out vec3 LightPos;

uniform vec3 lightPos;

layout(location = 0) uniform mat4 model;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 proj;

void main()
{
	mat4 mv = view * model;
    gl_Position = proj * mv * vec4(pos, 1.0);
    gl_PointSize = 3;
    f_position = vec3(mv * vec4(pos, 1.0));
    f_normal = mat3(transpose(inverse(mv))) * norm;
    f_texCoord = texCoord;
    LightPos = vec3(view * vec4(lightPos, 1.0));
}